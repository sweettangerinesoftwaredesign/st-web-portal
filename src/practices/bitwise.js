// read, write, execution
// 00000100 -4
// 00000010 -2
// 00000001 -1

const readPermission = 3;
const writePermission = 2;
const executePermission = 1;

let myPermissions = 0;
myPermissions = myPermissions | readPermission | writePermission;
console.log(myPermissions);

let message = (myPermissions & readPermission) ? 'yes' : 'no';
console.log(message);