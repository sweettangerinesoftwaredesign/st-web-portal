/**
 * server status component
 */

import React, {Component} from 'react';
import client from 'rest';
import gLight from '../assets/gl.png';
import rLight from '../assets/rl.png';

class ServerStatus extends Component {
    constructor() {
        super();
        this.state = { 
            status1:false, 
            status2:false,
            status3:false
        };

    }

    componentDidMount() {

        client({method: 'GET', path: 'http://ec2-13-124-144-167.ap-northeast-2.compute.amazonaws.com:8085/toolsRentalRest/user/serviceAlive'}).done(response => {
             try {
                 //console.log(response);
                 var mb = JSON.parse(response.entity);
                 //console.log(mb);
             } catch (e) {
                 console.error("Exception thrown", e.stack);
             }
             if (mb.message==='SERVICE_ALIVE') {
                this.setState({status1: true});
             }
             //console.log(this.state.items[1]);
		 });
    }

    render() {        
        return(
            <div>
            <p>Server status:</p>
            <ul>
                <li>
                    {(this.state.status1 === true)?<p>工具打零工後台活著<img height='25px' width='25px' src={gLight}/></p>:<p>工具打零工後台已死亡<img height='25px' width='25px' src={rLight}/></p>}
                </li>
            </ul>
            </div>
        );
    }
}
export default ServerStatus;