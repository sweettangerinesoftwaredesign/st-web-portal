/**
 * team page
 */
import React, {Component} from 'react';
import head1 from '../assets/head.png';
import head2 from '../assets/head2.jpg';
class Team extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        //let classes = this.getBadheClasses();
        //this.props.handleClose;
        //console.log(this.props.handleClose);
        this.props.onSetOpen(false);
        return (
            <div>
            <h3>茂谷柑團隊</h3>
            <p class='paraNoIndent'>
            負責人邦邦
            <br/>
            <img width='100px' height='100px' src={head1}/>
            </p>
            <p class='paraSmall'>
            在Web app以及Android app, 還有Java enterprise solutions，
            打滾數十年。
            曾任職於資管，資工，傳產，新創等不同領域。
            豐富的經驗帶領團隊成長前進。
            <br/>
            <a href='https://www.facebook.com/jerry.chen.73550794'>臉書連結</a>
            </p>
            
            <p class='paraNoIndent1'>
            小87白白
            <br/>
            <img width='20%' height='20%' src={head2}/>
            </p>
            <p class='paraSmall1'>
            從今天開始努力的大學生
            <br/>
            <a href='https://www.facebook.com/'>臉書連結不會用</a>
            <br/>
            <p>佔實先這樣</p>
            </p>
            </div>
        );
    }
}
export default Team;