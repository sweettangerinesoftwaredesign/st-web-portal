import React, {Component} from 'react';
import Counter from './counter';
class Counters extends Component {

    render() {
        //let classes = this.getBadheClasses();
        const {onReset, counters, onDelete, onIncrement} = this.props;
        return (
            <div>
                <button 
                onClick={this.props.onReset}
                className="btn btn-primary btn-sm m-2">Reset</button>
                {this.props.counters.map(counter=>
                <Counter key={counter.id} counter={counter} id={counter.id} onDelete={this.props.onDelete} 
                    onIncrement={this.props.onIncrement}
                    value={counter.value} selected={true}>
                    <h4>Counter #{counter.id}</h4>
                </Counter>
                    )}
            </div>
        );
    }
}
export default Counters;