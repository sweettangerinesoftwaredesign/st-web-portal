import React, {Component} from 'react';

class Counter extends Component {
    state = {
        //count : this.props.value, 
        tags: ['tag1', 'tag2', 'tag3']
        //imgUrl : 'https://picsum.photos/200'

    };

    componentDidUpdate(previousProps, previousState) {
        //console.log('previousProps', previousProps);
        //console.log('previousState', previousState);
    }

    componentWillUnmount() {
        console.log('unmount');
    }

    //constructor() {
    //    super();
        //console.log("cons", this);
    //    this.handleIncrement = this.handleIncrement.bind(this);
    //}

    styles = {
        fontSize:10,
        fontWeight:'bold'
    };

    renderTags() {
        if (this.state.tags.length === 0) {
            return <p>there are no tags</p>;
        }
        return <ur>{this.state.tags.map(tag=><li key={tag}>{tag}</li>)}</ur>;
    }

    //handleIncrement() {
    //    console.log('increment clicked', this);
    //}

    //handleIncrement = () => {
        //console.log('increment clicked', this);
        //this.props.value = 0; //read only
        //this.setState({count:this.state.count + 1});
    //};

    render() {
        //let classes = this.getBadheClasses();
        //console.log('props', this.props);

        return (
            // <React.Fragment>
            <div>
                {/* <img src={this.state.imgUrl}/> */}
                {/* <span style={{fontSize:20}} className="badge badge-primary m-2">{this.formatCount()}</span> */}
                {this.props.children}
                <span style={this.styles} className={this.getBadheClasses()}>{this.formatCount()}</span>
                {/* <button onClick={this.handleIncrement} className="btn btn-secondary btn-sm">increment</button> */}
                <button onClick={()=>this.props.onIncrement(this.props.counter)} className="btn btn-secondary btn-sm">increment</button>
                <button onClick={()=>this.props.onDelete(this.props.id)} className="btn btn-danger btn-sm m-2">delete</button>
                {/* <button onClick={()=>this.handleIncrement(product)} className="btn btn-secondary btn-sm">increment</button> */}
                {/*<ur>
                    {this.state.tags.map(tag=><li key={tag}>{tag}</li>)}
                </ur> */}
                {/*{this.state.tags.length === 3 && 'please create new tags' && 1}
                {this.renderTags()}*/}
            </div>
            //</React.Fragment>
        );
    }

    getBadheClasses() {
        let classes = "badge m-2 badge-";
        classes += this.props.counter.value === 0 ? "warning" : "primary";
        return classes;
    }

    formatCount() {
        const { value } = this.props.counter;
        return value === 0 ? "Zero" : value;
    }
}
export default Counter;