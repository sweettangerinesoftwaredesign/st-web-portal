/**
 * about page
 */
import React, {Component} from 'react';
class About extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        //let classes = this.getBadheClasses();
        //this.props.handleClose;
        //console.log(this.props.handleClose);
        this.props.onSetOpen(false);
        return (
            <div>
            <h3>關於茂谷柑</h3>
            <p class='privacyP'>
                一、我們的信念
                <br/>
                <p class='privacyC'>
                程式開發是一個珍貴的投資因此我們承諾選用可長久可持續經營的程式碼以及框架來完成作品，
                並且在技術上不斷的創新。
                </p>
            </p>
            <p class='privacyP'>
                二、福利制度
                <br/>
                <p class='privacyC'>
                加入茂谷柑共同創業的夥伴， 我們信仰勞動所得主要由勞動者所共同分配(採取虛擬股份制度)。
                <br/>
                同時我們信任員工的自我管理能力，自我創造力。
                </p>
            </p>
            </div>
        );
    }
}
export default About;