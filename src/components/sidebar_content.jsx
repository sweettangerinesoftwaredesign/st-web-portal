import React, {Component} from "react";
import PropTypes from "prop-types";
import MaterialTitlePanel from "./material_title_panel";
//import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import ReactDOM from 'react-dom';
import Product from './product';
import Team from './team';
import About from './about';
import Privacy from './privacy'

const styles = {
  sidebar: {
    width: 256,
    height: "100%"
  },
  sidebarLink: {
    display: "block",
    padding: "16px 0px",
    color: "#757575",
    textDecoration: "none"
  },
  divider: {
    margin: "8px 0",
    height: 1,
    backgroundColor: "#757575"
  },
  content: {
    padding: "16px",
    height: "100%",
    backgroundColor: "white"
  }
};

class SidebarContent extends Component {
    constructor(props) {
        super(props);
    }

    // handleClose = () => {
    //   this.props.sideBarOpen = false;
    // }

    product = () => {
      ReactDOM.render(<Product onSetOpen={this.props.onSetOpen} />, document.getElementById('content'));
    }

    about = () => {
      ReactDOM.render(<About onSetOpen={this.props.onSetOpen} />, document.getElementById('content'));
    }

    team = () => {
      ReactDOM.render(<Team onSetOpen={this.props.onSetOpen} />, document.getElementById('content'));
    }

    privacy = () => {
      ReactDOM.render(<Privacy onSetOpen={this.props.onSetOpen} />, document.getElementById('content'));
    }

    render() {   
     console.log(this.props.onSetOpen);
      const style = this.props.style
      ? { ...styles.sidebar, ...this.props.style }
      : styles.sidebar;

      const links = [];

      for (let ind = 0; ind < 10; ind++) {
        links.push(
          <a key={ind} href="#" style={styles.sidebarLink}>
            Mock menu item {ind}
          </a>
        );
      }

      return (
        <MaterialTitlePanel title="目錄" style={style}>
          <div style={styles.content}>
            <a href="index.html" style={styles.sidebarLink}>
              首頁
            </a>
            {/*<a href="#" onClick={this.product} style={styles.sidebarLink}>
              作品
            </a>*/}
            <div style={styles.divider} />
            {/* {links} */}
            {/*<a href="#" onClick={this.team} style={styles.sidebarLink}>
              團隊介紹
            </a>*/}
            {/*<a href="#" onClick={this.about} style={styles.sidebarLink}>
              關於我們
            </a>*/}
            {/*<div style={styles.divider} />*/}
            <a href="#" onClick={this.privacy} style={styles.sidebarLink}>
              產品隱私權聲明
            </a>
            
            {/*<a href="#track_gear_pp" onClick={this.privacyPolicies} style={styles.sidebarLink}>
              Track Gear(Privacy Policies)
          </a>*/}
            {/*<Router>
                <Route path="/track_gear_pp" component={PrivacyPolicies} />
            </Router>*/}
          </div>
        </MaterialTitlePanel>
      );
    }
}

SidebarContent.propTypes = {
  style: PropTypes.object
};

export default SidebarContent;
