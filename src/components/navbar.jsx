import React, {Component} from 'react';
import stLogo from '../assets/logo_80.png';

// stateless functional component
/* const NavBar = props => {
    return (
            <nav className="navbar navbar-light bg-light">
            <a className="navbar-brand" href="#">Navbar <span className="badge badge-pill badge-secondary">{props.totalCounters}</span>
            </a>
            </nav>
        );
} */

const NavBar = ({totalCounters}) => {
    return (
            <nav className="navbar navbar-light bg-light">
            <img  src={stLogo} alt="st logo"/>
            <a className="navbar-brand" href="#">茂谷柑軟體設計有限公司 <span className="badge badge-pill badge-secondary">{totalCounters}</span>
            </a>
            </nav>
        );
}
/* class NavBar extends Component {
    state = {
       
    };


    render() {
        //let classes = this.getBadheClasses();

        
    }
} */
export default NavBar;