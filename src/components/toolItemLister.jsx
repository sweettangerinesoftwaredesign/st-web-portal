import React, {Component} from 'react';
import client from 'rest';
//import Glide from 'react-glide';
//import express from 'express';
//import cors from 'cors';
//var app = express();
//const client = require('./client');

class ItemLister extends Component {
    constructor() {
        super();
        this.state = { items: [] };
        console.log('test');
    }

    componentDidMount() {
        console.log('mounted');

        //app.use(cors());
 
        //app.get('http://localhost:8085/toolsRentalRest/item/getLatestPosted?accMail="xxx"&lat=25&longt=121', function (req, res, next) {
        //    res.json({msg: 'This is CORS-enabled for all origins!'})
        //});

        client({method: 'GET', path: 'http://ec2-13-124-144-167.ap-northeast-2.compute.amazonaws.com:8085/toolsRentalRest/item/getLatestPosted?accMail="xxx"&lat=25&longt=121'}).done(response => {
             try {
                 //console.log(response.entity);
                 var tests = JSON.parse(response.entity);
                 console.log(tests);
             } catch (e) {
                 console.error("Exception thrown", e.stack);
             }
            
             this.setState({items: tests});
             //console.log(this.state.items[1]);
		 });

        // fetch(
        //     'http://ec2-13-125-42-104.ap-northeast-2.compute.amazonaws.com:8085/toolsRentalRest/item/getLatestPosted?accMail=xxx&lat=25&longt=121')
        // .then(response => {
        // if (response.ok) {
        //     console.log("res", response);
        //     return response.json();
        //     } else {
        //         console.log("err", response);
        //     throw new Error('Something went wrong ...');
        //     }
        // });

        //.then(data => this.setState({ data }));
        //console.log("items", this.state.items);
    }

    render() {        
        return(
            <div>
            <p class='paraNoIndent1'>
                工具打零工最新上傳:
                </p>
            <ul>
            {/* {this.state.items.map(item=>
                <img height="100" src={'http://ec2-13-125-42-104.ap-northeast-2.compute.amazonaws.com:8085/toolsRentalRest/file/files/' + item.files[0].realLocation} />
            )} */}
            {this.state.items.map(item=>
            <li key={item.toolItem.id}>
            <img height="100" src={'http://ec2-13-124-144-167.ap-northeast-2.compute.amazonaws.com:8085/toolsRentalRest/file/files/' + item.files[0].realLocation} />
            {item.toolItem.spec}</li>)}
            </ul>
            </div>
        );
    }
}

export default ItemLister;