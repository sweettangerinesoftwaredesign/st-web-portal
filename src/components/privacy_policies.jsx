/**
 * privacy page
 */
import React, {Component} from 'react';
class PrivacyPolicies extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        //let classes = this.getBadheClasses();
        //this.props.handleClose;
        //console.log(this.props.handleClose);
        //this.props.onSetOpen(false);
        return (
            <div>
                <h3>Track Gear App Privacy Policies</h3>
                <p class='privacyP'>
                    1. Collection of Location Information:
                    <br/>
                    <p class='privacyC'>
                        Our app must collect and use your precise location information, including GPS signals and other sensors, 
                    to provide location-based services and features. 
                    We only collect this information with your explicit consent and when it is necessary for the functioning of the app.
                    </p>
                </p>
                <p class='privacyP'>
                    2.  Use of Location Information:
                    <br/>
                    <p class='privacyC'>
                        The location information we collect is used to enhance your app experience and provide you with relevant content and features. 
                    This may include displaying speed records around Google map service.                    </p>
                </p>

                <p class='privacyP'>
                    3.  Sharing of Location Information:
                    <br/>
                    <p class='privacyC'>
                        We do not share your precise location information with any network service, this is firm, 
                    we don't have any backend network service collecting data. 
                    However, user can share your own location information with your trusted social networks.
                    </p>
                </p>

                <p class='privacyP'>
                    4.  Location Permissions:
                    <br/>
                    <p class='privacyC'>
                        To collect and use your location information, our app requires your explicit permission. 
                    You can enable or disable location services within your device's settings or through the app's own settings. 
                    Please note that disabling location services the functionalities of the app are totally useless.
                    </p>
                </p>
                <p class='privacyP'>
                    5.  Data Security:
                    <br/>
                    <p class='privacyC'>
                        We use local hive storage only, the app does not have any network service to send data out, 
                    that is guaranteed again.
                    </p>
                </p>
                <p class='privacyP'>
                    6.  User Controls and Choices:
                    <br/>
                    <p class='privacyC'>
                        You have the right to delete all location records collected in app, or just delete the entire app since hive db is locally used.
                    </p>
                </p>
                <p class='privacyP'>
                    7.  Updates to the Privacy Policy:
                    <br/>
                    <p class='privacyC'>
                        We may update our privacy policy from time to time to reflect changes in our practices or legal requirements. We will notify you of any significant updates through the app or other appropriate channels. It is advisable to review the privacy policy periodically for any changes that may affect your rights or usage of the app.
                    </p>
                </p>
                <p class='privacyP'>
                    8. The usage of app is for private only, 
                        we do not encourage illegal racing, breaking traffic laws, endangering public, please obey traffic laws, 
                    and please note SweetTangerines Software Inc. do not have any responsibility to individual actions.
                </p>
            </div>
        );
    }
}
export default PrivacyPolicies;