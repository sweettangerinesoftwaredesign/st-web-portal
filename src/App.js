import React, { Component } from 'react';
//import logo from './logo.svg';
import './App.css';
//import Navbar from './components/navbar'
//import Counters from './components/counters';
//import DrawerMenu from './components/drawerMenu';dddd
import stLogo from './assets/logo_80.png';
import menu from './assets/menu.png';
import Sidebar from "react-sidebar";
import ItemLister from './components/toolItemLister';
import ServerStatus from './components/serverStatus';
import MaterialTitlePanel from "./components/material_title_panel";
import SidebarContent from "./components/sidebar_content";
import { Server } from 'http';
import imgOffice from './assets/imgOffice.jpg';
import PrivacyPolicies from "./components/privacy_policies";
import { BrowserRouter as Router, Route } from 'react-router-dom';
import ReactDOM from 'react-dom';
//var ReactHighcharts = require('react-highcharts');
//const ReactHighstock = require('react-highcharts/ReactHighstock.src');

const styles = {
  contentHeaderMenuLink: {
    textDecoration: "none",
    color: "white",
    padding: 8,
    fontSize: "50px"
  },
  content: {
    padding: "16px"
  }
};

class App extends Component {
  constructor(props) {
    super(props);
    console.log(props);
    this.state = {
      counters:[ 
            {id:1, value:0}, 
            {id:2, value:3}, 
            {id:3, value:0}, 
            {id:4, value:0}],
      docked: false,
      open: false,
      transitions: true,
      touch: true,
      shadow: true,
      pullRight: false,
      touchHandleWidth: 20,
      dragToggleDistance: 30
    };
    this.renderPropCheckbox = this.renderPropCheckbox.bind(this);
    this.renderPropNumber = this.renderPropNumber.bind(this);
    this.onSetOpen = this.onSetOpen.bind(this);
    this.menuButtonClick = this.menuButtonClick.bind(this);
  }  
  componentDidMount() {
    //ajax call
    console.log("app mounted");
  }

  privacyPolicies = () => {
    ReactDOM.render(<PrivacyPolicies/>, document.getElementById('content'));
  }

  componentWillMount() {
  }
 
  onSetOpen(open) {
    this.setState({ open });
  }

  menuButtonClick(ev) {
    ev.preventDefault();
    this.onSetOpen(!this.state.open);
  }

  renderPropCheckbox(prop) {
    const toggleMethod = ev => {
      const newState = {};
      newState[prop] = ev.target.checked;
      this.setState(newState);
    };

    return (
      <p key={prop}>
        <label htmlFor={prop}>
          <input
            type="checkbox"
            onChange={toggleMethod}
            checked={this.state[prop]}
            id={prop}
          />
          {prop}
        </label>
      </p>
    );
  }

  renderPropNumber(prop) {
    const setMethod = ev => {
      const newState = {};
      newState[prop] = parseInt(ev.target.value, 10);
      this.setState(newState);
    };

    return (
      <p key={prop}>
        {prop}{" "}
        <input type="number" onChange={setMethod} value={this.state[prop]} />
      </p>
    );
  }

  handleReset = () => {
      const counters = this.state.counters.map(c=>{
          c.value = 0;
          return c;
      });
      this.setState({counters});
  }

  handleIncrement = counter => {
      //console.log("delete event logged", counter);
      const counters = [...this.state.counters];
      const index = counters.indexOf(counter);
      counters[index] = {...counter};
      counters[index].value++;
      this.setState({counters});
      console.log(this.state.counters[index]);
  }

  handleDelete = counterId => {
      console.log("delete event logged", counterId);
      const counters = this.state.counters.filter(c => c.id !== counterId);
      //this.setState({counters : counters});
      this.setState({counters});
  }

  render() {
    console.log("rendered");
    const config = {
                xAxis: {
                  categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
                },
                series: [{
                  data: [29.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 295.6, 454.4]
                }]
              };
    const data1 = [
                [1220832000000, 22.56], [1220918400000, 21.67], [1221004800000, 21.66], [1221091200000, 21.81], [1221177600000, 21.28], [1221436800000, 20.05], [1221523200000, 19.98], [1221609600000, 18.26], [1221696000000, 19.16], [1221782400000, 20.13], [1222041600000, 18.72], [1222128000000, 18.12], [1222214400000, 18.39], [1222300800000, 18.85], [1222387200000, 18.32], [1222646400000, 15.04], [1222732800000, 16.24], [1222819200000, 15.59], [1222905600000, 14.3], [1222992000000, 13.87], [1223251200000, 14.02], [1223337600000, 12.74], [1223424000000, 12.83], [1223510400000, 12.68], [1223596800000, 13.8], [1223856000000, 15.75], [1223942400000, 14.87], [1224028800000, 13.99], [1224115200000, 14.56], [1224201600000, 13.91], [1224460800000, 14.06], [1224547200000, 13.07], [1224633600000, 13.84], [1224720000000, 14.03], [1224806400000, 13.77], [1225065600000, 13.16], [1225152000000, 14.27], [1225238400000, 14.94], [1225324800000, 15.86], [1225411200000, 15.37], [1225670400000, 15.28], [1225756800000, 15.86], [1225843200000, 14.76], [1225929600000, 14.16], [1226016000000, 14.03], [1226275200000, 13.7], [1226361600000, 13.54], [1226448000000, 12.87], [1226534400000, 13.78], [1226620800000, 12.89], [1226880000000, 12.59], [1226966400000, 12.84], [1227052800000, 12.33], [1227139200000, 11.5], [1227225600000, 11.8], [1227484800000, 13.28], [1227571200000, 12.97], [1227657600000, 13.57], [1227830400000, 13.24], [1228089600000, 12.7], [1228176000000, 13.21], [1228262400000, 13.7], [1228348800000, 13.06], [1228435200000, 13.43], [1228694400000, 14.25], [1228780800000, 14.29], [1228867200000, 14.03], [1228953600000, 13.57], [1229040000000, 14.04], [1229299200000, 13.54]
              ];

    const config1 = {
      rangeSelector: {
        selected: 1
      },
      title: {
        text: 'AAPL Stock Price'
      },
      series: [{
        name: 'AAPL',
        data: data1,
        tooltip: {
          valueDecimals: 2
        }
      }]
    };

    const sidebar = <SidebarContent onSetOpen={this.onSetOpen}/>;

    const contentHeader = (
      <span>
        {!this.state.docked && (
          <a
            onClick={this.menuButtonClick}
            href="#"
            style={styles.contentHeaderMenuLink}
          >
            <img src={menu} width='52px' height='52px'  alt="menu"/>
          </a>
        )}

        <img src={stLogo} alt="st logo"/>
        <p>茂谷柑軟體設計有限公司</p>
      </span>
    );

    const sidebarProps = {
      sidebar,
      docked: this.state.docked,
      sidebarClassName: "custom-sidebar-class",
      contentId: "custom-sidebar-content-id",
      open: this.state.open,
      touch: this.state.touch,
      shadow: this.state.shadow,
      pullRight: this.state.pullRight,
      touchHandleWidth: this.state.touchHandleWidth,
      dragToggleDistance: this.state.dragToggleDistance,
      transitions: this.state.transitions,
      onSetOpen: this.onSetOpen
    };
    return (
      <Sidebar {...sidebarProps}>
        <MaterialTitlePanel title={contentHeader}>
          <div style={styles.content}>
            <React.Fragment>
            {/* <Navbar totalCounters={this.state.counters.filter(c=> c.value > 0).length} /> */}
              <main id="content">
                {/* <Counters counters={this.state.counters} onReset={this.handleReset} onDelete={this.handleDelete} onIncrement={this.handleIncrement} /> */}
                <img src={imgOffice} />
            	{/*<ServerStatus/>
                <ItemLister/>*/}
              </main>
            {/* <ReactHighcharts config={config}/> */}
            {/* <ReactHighstock config={config1}/> */}
            </React.Fragment>
            <Router>
                <Route path="/track_gear_pp" component={PrivacyPolicies} />
            </Router>
            {/* <p>
              React Sidebar is a sidebar component for React. It offers the
              following features:
            </p>
            <ul>
              <li>Have the sidebar slide over main content</li>
              <li>Dock the sidebar next to the content</li>
              <li>Touch enabled: swipe to open and close the sidebar</li>
              <li>
                Easy to combine with media queries for auto-docking (
                <a href="responsive_example.html">see example</a>)
              </li>
              <li>
                Sidebar and content passed in as PORCs (Plain Old React
                Components)
              </li>
              <li>
                <a href="https://github.com/balloob/react-sidebar">
                  Source on GitHub
                </a>{" "}
                (MIT license)
              </li>
              <li>Only dependency is React</li>
            </ul>
            <p>
              <a href="https://github.com/balloob/react-sidebar#installation">
                Instructions how to get started.
              </a>
            </p>
            <p>
              <b>Current rendered sidebar properties:</b>
            </p>
            {[
              "open",
              "docked",
              "transitions",
              "touch",
              "shadow",
              "pullRight"
            ].map(this.renderPropCheckbox)}
            {["touchHandleWidth", "dragToggleDistance"].map(
              this.renderPropNumber
            )} */}
          </div>
        </MaterialTitlePanel>
      </Sidebar>
        
    );
  }
}

export default App;
