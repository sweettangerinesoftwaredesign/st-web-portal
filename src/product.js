import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import 'bootstrap/dist/css/bootstrap.css';
//import ReactHighcharts from 'react-highcharts';
//import Counter from './components/counter';
//import Counters from './components/counters';

//const element1 = <h1>Hello there!</h1>;
//console.log(element1);


ReactDOM.render(<App/>, document.getElementById('root'));
//ReactDOM.render(element1, document.getElementById('root'));
registerServiceWorker();

